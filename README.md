# vue-fractional-calculator

> Test project

## Build Setup

``` bash
# install dependencies
yarn

# serve with hot reload at localhost:8080
yarn dev

# build for production with minification
yarn build

# build for production and view the bundle analyzer report
yarn build --report

# run unit tests
yarn unit

# run all tests
yarn test
```

## Use in project
``` bash
- Vue
- Vuex
- Vue CLI
- bootstrap-vue
- LocalStorage
- mathjs
- scss
- bootstrap 4
- yarn
