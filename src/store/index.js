import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import calc from './calc'

Vue.use(Vuex)

export const storeConfig = {
  plugins: [createPersistedState()],
  modules: {
    calc
  },
  state: {
    routerTransitionName: 'slide-right'
  },
  mutations: {
    SET_ROUTER_TRANSITION_NAME (state, name) {
      state.routerTransitionName = name
    }
  },
  actions: {

  },
  getters: {

  }
}
export default new Vuex.Store(storeConfig)
