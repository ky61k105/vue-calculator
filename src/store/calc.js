import math from 'mathjs'
export const state = {
  countFractions: 2,
  operationTypes: [
    '+',
    '-',
    '*',
    '/'
  ],
  expression: [
    {
      type: 'fraction',
      value: {
        n: '',
        d: '',
        s: 1
      }
    },
    {
      type: 'operation',
      value: 0
    },
    {
      type: 'fraction',
      value: {
        d: '',
        n: '',
        s: 1
      }
    }
  ],
  result: null,
  history: []
}

export const mutations = {
  SET_EXPRESSION_ELEMENT (state, { value, i }) {
    state.expression[i].value = value
    state.expression = [...state.expression]
  },
  SET_EXPRESSION_ARRAY (state, arr) {
    state.expression = arr
  },
  SET_COUNT_FRACTIONS (state, count) {
    state.countFractions = count
  },
  SET_RESULT (state, result) {
    state.result = result
  },
  SET_HISTORY (state, items) {
    state.history = items
  },
  UNSHIFT_TO_HISTORY (state, item) {
    state.history.unshift(item)
  },
  CLEAR_HISTORY (state) {
    state.history = []
  }
}

export const getters = {
  countEmptyFractions (state) {
    return state.expression.reduce((count, element) => {
      if (element.type !== 'fraction') {
        return count
      }
      if (!element.value.n || !element.value.d) {
        return count + 1
      }
      return count
    }, 0)
  },
  expressionAsString (state, getters) {
    if (getters.countEmptyFractions !== 0) {
      return ''
    }
    return state.expression.reduce((result, element) => {
      if (element.type === 'fraction') {
        return result + (element.value.s > 0 ? '' : '-') + element.value.n + '/' + element.value.d
      } else {
        return result + ' ' + state.operationTypes[element.value] + ' '
      }
    }, '')
  },
  resultFraction (state) {
    return math.fraction(state.result)
  }
}

export const actions = {
  setExpressionElement ({ commit }, data) {
    commit('SET_EXPRESSION_ELEMENT', data)
    commit('SET_RESULT', null)
  },
  calculate ({state, getters, commit, dispatch}) {
    if (getters.countEmptyFractions) {
      return false
    }
    commit('SET_RESULT', math.eval(getters.expressionAsString))
    commit('UNSHIFT_TO_HISTORY', {
      expressionAsString: getters.expressionAsString,
      resultFraction: getters.resultFraction
    })
  },
  setCountFractions ({state, commit}, count) {
    count = parseInt(count)
    if (state.countFractions === count) {
      return false
    }

    if (state.countFractions > count) {
      commit('SET_EXPRESSION_ARRAY', state.expression.slice(0, count * 2 - 1))
    } else {
      let countToPush = count - state.countFractions
      let arr = state.expression
      for (let i = 0; i < countToPush; i++) {
        arr.push({
          type: 'operation',
          value: 0
        })
        arr.push({
          type: 'fraction',
          value: {
            d: '',
            n: '',
            s: 1
          }
        })
      }
      commit('SET_EXPRESSION_ARRAY', arr)
    }
    commit('SET_COUNT_FRACTIONS', count)
    commit('SET_RESULT', null)
  },
  resetCalc ({state, commit}) {
    let arr = state.expression.map((element) => {
      if (element.type === 'fraction') {
        return {
          type: 'fraction',
          value: {
            d: '',
            n: '',
            s: 1
          }
        }
      } else {
        return element
      }
    })
    commit('SET_EXPRESSION_ARRAY', arr)
    commit('SET_RESULT', null)
  },
  clearHistory ({commit}) {
    commit('CLEAR_HISTORY')
  },
  setHistory ({commit}, history) {
    commit('SET_HISTORY', history)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
