import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

const IndexPage = () => import('@/components/pages/IndexPage')
const HistoryPage = () => import('@/components/pages/HistoryPage')

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'IndexPage',
      component: IndexPage
    },
    {
      path: '/history',
      name: 'HistoryPage',
      component: HistoryPage
    }
  ]
})

router.beforeEach((to, from, next) => {
  let toDepth = 1
  if (to.path !== '/') {
    toDepth = to.path.split('/').length
  }
  let fromDepth = 1
  if (from.path !== '/') {
    fromDepth = from.path.split('/').length
  }
  store.commit('SET_ROUTER_TRANSITION_NAME', toDepth < fromDepth ? 'slide-right' : 'slide-left')
  next()
})

export default router
