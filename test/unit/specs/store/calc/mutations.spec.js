import {createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import {cloneDeep} from 'lodash'
import {storeConfig} from '@/store'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Vuex check mutations [namespace: calc]', () => {
  let store
  let calcState
  beforeEach(() => {
    localStorage.clear()
    store = new Vuex.Store(cloneDeep(storeConfig))
    calcState = store.state.calc
  })

  it('SET_EXPRESSION_ELEMENT', () => {
    let payload = {
      value: {
        n: 10,
        d: 11,
        s: 1
      },
      i: 0
    }
    store.commit('calc/SET_EXPRESSION_ELEMENT', payload)
    expect(calcState.expression[0]).to.deep.equal({
      type: 'fraction',
      value: {
        n: 10,
        d: 11,
        s: 1
      }
    })
  })

  it('SET_EXPRESSION_ARRAY', () => {
    let payload = [
      {
        type: 'fraction',
        value: {
          n: 10,
          d: 11,
          s: 1
        }
      },
      {
        type: 'operation',
        value: 0
      },
      {
        type: 'fraction',
        value: {
          n: 1,
          d: 3,
          s: 1
        }
      }
    ]
    store.commit('calc/SET_EXPRESSION_ARRAY', payload)
    expect(calcState.expression).to.deep.equal(payload)
  })

  it('SET_COUNT_FRACTIONS', () => {
    let payload = 3
    store.commit('calc/SET_COUNT_FRACTIONS', payload)
    expect(calcState.countFractions).to.deep.equal(payload)
  })

  it('SET_RESULT', () => {
    let payload = 3
    store.commit('calc/SET_RESULT', payload)
    expect(calcState.result).to.deep.equal(payload)
  })

  it('SET_HISTORY', () => {
    expect(calcState.history).to.deep.equal([])
    let payload = [
      {
        date: new Date().getTime(),
        expression: calcState.expression,
        expressionAsString: store.getters['calc/expressionAsString'],
        resultFraction: store.getters['calc/resultFraction']
      }
    ]
    store.commit('calc/SET_HISTORY', payload)
    expect(calcState.history).to.deep.equal(payload)
  })

  it('UNSHIFT_TO_HISTORY', () => {
    expect(calcState.history).to.deep.equal([])
    let payload = {
      date: new Date().getTime(),
      expression: calcState.expression,
      expressionAsString: store.getters['calc/expressionAsString'],
      resultFraction: store.getters['calc/resultFraction']
    }

    let payload2 = {
      date: new Date().getTime() + 1,
      expression: calcState.expression,
      expressionAsString: store.getters['calc/expressionAsString'],
      resultFraction: store.getters['calc/resultFraction']
    }

    store.commit('calc/UNSHIFT_TO_HISTORY', payload)
    store.commit('calc/UNSHIFT_TO_HISTORY', payload2)
    expect(calcState.history[0]).to.deep.equal(payload2)
  })
  it('CLEAR_HISTORY', () => {
    expect(calcState.history).to.deep.equal([])
    let payload = {
      date: new Date().getTime(),
      expression: calcState.expression,
      expressionAsString: store.getters['calc/expressionAsString'],
      resultFraction: store.getters['calc/resultFraction']
    }
    store.commit('calc/UNSHIFT_TO_HISTORY', payload)
    expect(calcState.history[0]).to.deep.equal(payload)
    store.commit('calc/CLEAR_HISTORY')
    expect(calcState.history).to.deep.equal([])
  })
})
