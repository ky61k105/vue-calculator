import {createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import {cloneDeep} from 'lodash'
import {storeConfig} from '@/store'
import math from 'mathjs'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Vuex check getters [namespace: calc]', () => {
  let store = new Vuex.Store(cloneDeep(storeConfig))
  beforeEach(() => {
    localStorage.clear()
  })
  it('countEmptyFractions should be 1', () => {
    store.commit('calc/SET_EXPRESSION_ELEMENT', {
      value: {
        n: 1,
        d: 2,
        s: 1
      },
      i: 0
    })
    store.getters['calc/countEmptyFractions'].should.be.equal(1)
  })

  it('countEmptyFractions should be 0', () => {
    store.commit('calc/SET_EXPRESSION_ELEMENT', {
      value: {
        n: 1,
        d: 3,
        s: 1
      },
      i: 2
    })
    store.getters['calc/countEmptyFractions'].should.be.equal(0)
  })

  it('expressionAsString should be equal "1/2 + 1/3"', () => {
    store.getters['calc/expressionAsString'].should.be.equal('1/2 + 1/3')
  })

  describe('If store.state.calc.result x', () => {
    let results = [
      1,
      1.1666666666666665,
      1.8333333333333333
    ]
    results.map(result => {
      it('x = ' + result + ' resultFraction getters["calc/resultFraction"] should be equal math.fraction("' + result + '")', () => {
        store.commit('calc/SET_RESULT', result)
        expect(store.getters['calc/resultFraction']).to.deep.equal(math.fraction(result))
      })
    })
  })
})
