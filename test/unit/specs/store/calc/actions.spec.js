import {createLocalVue} from '@vue/test-utils'
import Vuex from 'vuex'
import {cloneDeep} from 'lodash'
import {storeConfig} from '@/store'
import math from 'mathjs'
const localVue = createLocalVue()

localVue.use(Vuex)

describe('Vuex check actions [namespace: calc]', () => {
  let store = new Vuex.Store(cloneDeep(storeConfig))
  beforeEach(() => {
    localStorage.clear()
  })
  describe('dispatch("calc/setExpressionElement")', () => {
    it('set first fraction', () => {
      store.dispatch('calc/setExpressionElement', {
        value: {
          n: 1,
          d: 2,
          s: 1
        },
        i: 0
      })
      expect(store.state.calc.expression[0]).to.deep.equal({
        type: 'fraction',
        value: {
          n: 1,
          d: 2,
          s: 1
        }
      })
    })

    it('set second fraction', () => {
      store.dispatch('calc/setExpressionElement', {
        value: {
          n: 1,
          d: 3,
          s: 1
        },
        i: 2
      })
      expect(store.state.calc.expression[2]).to.deep.equal({
        type: 'fraction',
        value: {
          n: 1,
          d: 3,
          s: 1
        }
      })
    })

    it('set operation', () => {
      store.dispatch('calc/setExpressionElement', {
        value: 2,
        i: 1
      })
      expect(store.state.calc.expression[1]).to.deep.equal({
        type: 'operation',
        value: 2
      })
    })
  })

  describe('dispatch("calc/setCountFractions")', () => {
    it('set count fractions = 3', () => {
      store.dispatch('calc/setCountFractions', 3)
      expect(store.state.calc.countFractions).to.equal(3)
    })

    it('count empty fractions should be equal 1', () => {
      store.dispatch('calc/setCountFractions', 3)
      expect(store.getters['calc/countEmptyFractions']).to.be.equal(1)
    })
  })

  describe('dispatch("calc/setExpressionElement")', () => {
    it('set 3th fraction', () => {
      store.dispatch('calc/setExpressionElement', {
        value: {
          n: 1,
          d: 4,
          s: 1
        },
        i: 4
      })
      expect(store.state.calc.expression[0]).to.deep.equal({
        type: 'fraction',
        value: {
          n: 1,
          d: 2,
          s: 1
        }
      })
    })

    it('count empty fractions should be equal 0', () => {
      expect(store.getters['calc/countEmptyFractions']).to.be.equal(0)
    })

    it('getters["calc/expressionAsString"] should be equal "1/2 * 1/3 + 1/4"', () => {
      expect(store.getters['calc/expressionAsString']).to.be.equal('1/2 * 1/3 + 1/4')
    })
  })

  describe('dispatch("calc/calculate")', () => {
    it('store.state.calc.result should be equal 0.41666666666666663', () => {
      store.dispatch('calc/calculate')
      expect(store.state.calc.result).to.be.equal(0.41666666666666663)
    })

    it('getters["calc/resultFraction"] should be equal should be equal math.fraction(0.41666666666666663)', () => {
      expect(store.getters['calc/resultFraction']).to.be.deep.equal(math.fraction(0.41666666666666663))
    })

    it('store.state.calc.history.length should be equal 1', () => {
      expect(store.state.calc.history.length).to.be.equal(1)
    })

    it('check store.state.calc.history[0]', () => {
      expect(store.state.calc.history[0]).to.deep.equal({
        expressionAsString: '1/2 * 1/3 + 1/4',
        resultFraction: math.fraction(0.41666666666666663)
      })
    })
  })
})
