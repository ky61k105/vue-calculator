import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import { cloneDeep } from 'lodash'
import { storeConfig } from '@/store'

const localVue = createLocalVue()

localVue.use(Vuex)

describe('Vuex must have correct state object ', () => {
  let store = new Vuex.Store(cloneDeep(storeConfig))
  beforeEach(() => {
    localStorage.clear()
  })
  describe('state.routerTransitionName ', () => {
    it('should be a string', () => {
      store.state.routerTransitionName.should.be.a('string')
    })

    it('should be a "slide-right" by default', () => {
      store.state.routerTransitionName.should.equal('slide-right')
    })
  })

  describe('[namespace: calc]', () => {
    let calcState = store.state.calc

    describe('state.countFractions ', () => {
      it('should be a number', () => {
        calcState.countFractions.should.be.an('number')
      })
      it('should be equal 2 by default', () => {
        calcState.countFractions.should.be.equal(2)
      })
    })

    describe('state.operationTypes ', () => {
      it('should be an array', () => {
        calcState.operationTypes.should.be.an('array')
      })

      it('should include "+", "-", "*", "/"', () => {
        calcState.operationTypes.should.to.have.members(['+', '-', '*', '/'])
      })

      it('should have length equal 4', () => {
        calcState.operationTypes.length.should.be.equal(4)
      })
    })

    describe('state.result ', () => {
      it('should be a null', () => {
        expect(calcState.result).to.be.equal(null)
      })
    })

    describe('state.history ', () => {
      it('should be an array', () => {
        calcState.history.should.be.an('array')
      })

      it('should have length equal 0', () => {
        calcState.history.length.should.be.equal(0)
      })
    })

    describe('state.expression ', () => {
      it('should be an array', () => {
        expect(calcState.expression).to.be.an('array')
      })

      it('should have length % 2 === 1', () => {
        expect(calcState.expression.length % 2).to.be.equal(1)
      })

      describe(' even element must be a fraction, odd element must be an operation', () => {
        calcState.expression.forEach((element, index) => {
          if (index % 2 === 0) {
            it('[' + index + '] should be fraction', () => {
              expect(element).to.deep.equal({
                type: 'fraction',
                value: {
                  d: '',
                  n: '',
                  s: 1
                }
              })
            })
          } else {
            it('[' + index + '] should be operation', () => {
              expect(element).to.deep.equal({
                type: 'operation',
                value: 0
              })
            })
          }
        })
      })
    })
  })
})
