import { shallowMount } from '@vue/test-utils'
import Fraction from '@/components/fractional-calculator/Fraction'

describe('components/fractional-calculator/Fraction.vue', () => {
  let propsDatas = [
    {
      value: {
        n: '1',
        d: '2',
        s: 1
      }
    },
    {
      value: {
        n: '1',
        d: '3',
        s: -1
      },
      readonly: false
    },
    {
      value: {
        n: '3',
        d: '4',
        s: -1
      },
      readonly: true
    }
  ]
  propsDatas.forEach(propsData => {
    describe('When fraction is equal ' + (propsData.value.s > 0 ? '' : '-') + propsData.value.n + '/' + propsData.value.d, () => {
      const wrapper = shallowMount(Fraction, {propsData: propsData})
      let inputs = wrapper.findAll('input')

      it('First input value should be a ' + propsData.value.n, () => {
        expect(inputs.at(0).element.value).to.equal(propsData.value.n)
      })

      it('Second input value should be a ' + propsData.value.d, () => {
        expect(inputs.at(1).element.value).to.equal(propsData.value.d)
      })

      it('If props.readonly == ' + propsData.readonly + ', first input should' + (propsData.readonly ? '' : 'n\'t') + ' be readonly', () => {
        let readonly = expect(inputs.at(0).attributes().readonly)
        if (propsData.readonly) {
          readonly.to.be.equal('readonly')
        } else {
          readonly.to.be.an('undefined')
        }
      })

      it('If props.readonly == ' + propsData.readonly + ', second input should' + (propsData.readonly ? '' : 'n\'t') + ' be readonly', () => {
        let readonly = expect(inputs.at(1).attributes().readonly)
        if (propsData.readonly) {
          readonly.to.be.equal('readonly')
        } else {
          readonly.to.be.an('undefined')
        }
      })

      it('Should show minus if fraction < 0', () => {
        if (propsData.value.s < 0) {
          expect(wrapper.find('.fraction').classes()).to.include('minus')
        } else {
          expect(wrapper.find('.fraction').classes()).does.not.include('minus')
        }
      })
    })
  })
})
