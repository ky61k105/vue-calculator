import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import { cloneDeep } from 'lodash'
import { storeConfig } from '@/store'
import FractionalCalculator from '@/components/fractional-calculator/Index'

const localVue = createLocalVue()

localVue.use(Vuex)
describe('components/fractional-calculator/Index.vue ', () => {
  let store = new Vuex.Store(cloneDeep(storeConfig))
  const wrapper = shallowMount(FractionalCalculator, { store, localVue })
  wrapper.find('input')
})
